# Config that is very close to a i3 window manager's keybinding.

set -s escape-time 0

setw -g aggressive-resize on

# First remove *all* keybindings
unbind-key -a

# List keys
bind-key ? list-keys

# Copy mode
bind-key [ copy-mode

# Paste buffer
bind-key ] paste-buffer

# Refresh client
bind-key r refresh-client \; display-message "Refresh client"

# Start with index 1
set -g base-index 1
setw -g pane-base-index 1


# Set new prefix

# Note : you can press super key by set M.
# (tested with tty only)
set -g prefix C-a
bind-key Space send-prefix

# Clock
setw -g clock-mode-style 24

# Config Reloads
bind R source-file ~/.config/tmux/tmux.conf \; display-message "Config reloaded"

# Mouse on/off
set -g mouse off

# Split window

bind-key z split-window -h -c "#{pane_current_path}"
bind-key x split-window -v -c "#{pane_current_path}"

# Rotate Window
bind-key -n M-o rotate-window

# Swap pane
bind-key -r L swap-pane -U
bind-key -r K swap-pane -D

# vim-like pane switching
bind -n M-e last-window
bind -n M-k select-pane -U
bind -n M-j select-pane -D
bind -n M-h select-pane -L
bind -n M-l select-pane -R

# Resize pane with Alt (prefix with repeat)
bind-key -r H resize-pane -L 5 \; display-message "Resize left"
bind-key -r J resize-pane -D 5 \; display-message "Resize down"
bind-key -r K resize-pane -U 5 \; display-message "Resize up"
bind-key -r L resize-pane -R 5 \; display-message "Resize right"

# Automatically set window title
set-window-option -g automatic-rename on
set-option -g set-titles on

# Choose Window
bind-key w choose-window

# Switch windows alt+number
bind-key -n M-1 if-shell 'tmux select-window -t 1' '' 'new-window -t 1'
bind-key -n M-2 if-shell 'tmux select-window -t 2' '' 'new-window -t 2'
bind-key -n M-3 if-shell 'tmux select-window -t 3' '' 'new-window -t 3'
bind-key -n M-4 if-shell 'tmux select-window -t 4' '' 'new-window -t 4'
bind-key -n M-5 if-shell 'tmux select-window -t 5' '' 'new-window -t 5'
bind-key -n M-6 if-shell 'tmux select-window -t 6' '' 'new-window -t 6'
bind-key -n M-7 if-shell 'tmux select-window -t 7' '' 'new-window -t 7'
bind-key -n M-8 if-shell 'tmux select-window -t 8' '' 'new-window -t 8'
bind-key -n M-9 if-shell 'tmux select-window -t 9' '' 'new-window -t 9'
bind-key -n M-0 if-shell 'tmux select-window -t 10' '' 'new-window -t 0'

# Change current pane to next window
bind-key -n M-! if-shell 'tmux join-pane -t :1' '' 'new-window -d -t 1; join-pane -t :1 ; kill-pane -a'
bind-key -n M-@ if-shell 'tmux join-pane -t :2' '' 'new-window -d -t 2; join-pane -t :2 ; kill-pane -a'
bind-key -n M-# if-shell 'tmux join-pane -t :3' '' 'new-window -d -t 3; join-pane -t :3 ; kill-pane -a'
bind-key -n M-$ if-shell 'tmux join-pane -t :4' '' 'new-window -d -t 4; join-pane -t :4 ; kill-pane -a'
bind-key -n M-% if-shell 'tmux join-pane -t :5' '' 'new-window -d -t 5; join-pane -t :5 ; kill-pane -a'
bind-key -n M-^ if-shell 'tmux join-pane -t :6' '' 'new-window -d -t 6; join-pane -t :6 ; kill-pane -a'
bind-key -n M-& if-shell 'tmux join-pane -t :7' '' 'new-window -d -t 7; join-pane -t :7 ; kill-pane -a'
bind-key -n M-* if-shell 'tmux join-pane -t :8' '' 'new-window -d -t 8; join-pane -t :8 ; kill-pane -a'
bind-key -n M-( if-shell 'tmux join-pane -t :9' '' 'new-window -d -t 9; join-pane -t :9 ; kill-pane -a'
bind-key -n M-) if-shell 'tmux join-pane -t :10' '' 'new-window -d -t 10; join-pane -t :10 ; kill-pane -a'

# Kill Selected Pane
bind-key -n M-q kill-pane


# color configuration
set-option -ga terminal-overrides ",xterm-256color:Tc"
#set inactive/active window styles
set -g window-style 'fg=colour247,bg=colour236'
set -g window-active-style 'fg=colour250,bg=black'
# set the pane border colors 
set -g pane-border-style 'fg=colour235,bg=colour238' 
set -g pane-active-border-style 'fg=colour51,bg=colour236'

# additions from the primeagen 
set-option -g default-terminal "screen-256color"
set -s escape-time 0

set -g status-style 'bg=#333333 fg=#5eacd3'
set -g base-index 1
set-window-option -g mode-keys vi

bind -T copy-mode-vi v send-keys -X begin-selection
bind -T copy-mode-vi y send-keys -X copy-pipe-and-cancel 'xclip -in -selection clipboard'

bind -r D neww -c "#{pane_current_path}" "[[ -e TODO.md ]] && nvim TODO.md || nvim ~/.dotfiles/personal/todo.md"

#Primeagen's godly scripts
# /* --------------------------------------------------------------------------------- */
# forget the find window.  That is for chumps
bind-key -r f run-shell "tmux neww ~/.config/bin/.local/scripts/tmux-sessionizer"

bind-key -r i run-shell "tmux neww tmux-cht.sh"
bind-key -r G run-shell "~/.local/bin/tmux-sessionizer ~/work/nrdp"
bind-key -r C run-shell "~/.local/bin/tmux-sessionizer ~/work/tvui"
bind-key -r R run-shell "~/.local/bin/tmux-sessionizer ~/work/milo"
# bind-key -r L run-shell "~/.local/bin/tmux-sessionizer ~/work/hpack"
bind-key -r H run-shell "~/.local/bin/tmux-sessionizer ~/personal/vim-with-me"
bind-key -r T run-shell "~/.local/bin/tmux-sessionizer ~/personal/refactoring.nvim"
bind-key -r N run-shell "~/.local/bin/tmux-sessionizer ~/personal/harpoon"
bind-key -r S run-shell "~/.local/bin/tmux-sessionizer ~/personal/developer-productivity"

